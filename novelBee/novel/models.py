from django.db import models


class NovelCategory(models.Model):
    class Meta:
        db_table = 'novel_category'
    title = models.CharField(max_length=30)
    photo_url = models.CharField(max_length=200, null=True, default=None, blank=True)


class NovelSubcategory(models.Model):
    class Meta:
        db_table = 'novel_subcategory'
    category_id = models.PositiveSmallIntegerField(default=0)
    title = models.CharField(max_length=30)
    photo_url = models.CharField(max_length=200, blank=True, null=True)


# 這個資料表混合兩種資料，1. ptt資料，2. app上的發文資料(我們網站才有的資料)
class Novel(models.Model):
    class Meta:
        db_table = 'novel'
    # 先定義兩邊都會用到的東西
    # source代表資料來源
    # 0是本網站有的
    # 1是ptt
    # 2是起點中文網
    source = models.PositiveSmallIntegerField(default=1) 
    title = models.CharField(max_length=60)
    user = models.CharField(max_length=30)
    user_id = models.PositiveIntegerField(default=0)
    category_id = models.PositiveSmallIntegerField(default=0)
    # class_id = models.ForeignKey(NovelClass, on_delete=models.SET_NULL, null=True)
    # sub_class = models.ForeignKey(NovelSubclass, on_delete=models.SET_NULL, null=True)
    subcategory_id = models.PositiveSmallIntegerField(default=0)
    update_time = models.DateTimeField(auto_now=True)
    # vote = models.PositiveIntegerField(default=0)
    vote = models.IntegerField(default=0)
    # 先定義ptt資料專用
    url = models.CharField(max_length=200, blank=True, null=True)
    # 隨意發文(FreePost)專用，預防以後每一類裡面可能會有獨立的隨意發文區
    # 上面的class_id=0用來表示不屬於任何一類的隨意發文區，1-14則代表對應類別的隨意發文區
    content = models.CharField(max_length=500, blank=True, null=True)
    # 20180620新增欄位
    is_delete = models.BooleanField(default=True)


class FreePostComment(models.Model):
    class Meta:
        db_table = 'free_post_comment'
    # user用來儲存會員名稱，user_id用來儲存編號，user_id才是用來搜尋的key
    user = models.CharField(max_length=30)
    user_id = models.PositiveIntegerField(default=0)
    comment = models.CharField(max_length=100)
    # 新增推文或噓文欄位comment_pos_normal_neg
    # 1 表示推文
    # -1 表示噓文
    # 0表示普通回文
    comment_pos_normal_neg = models.SmallIntegerField(default=0)
    # parent_id利用Novel的id來判斷是哪個隨意發文文章的回覆
    parent_id = models.PositiveIntegerField()
    update_time = models.DateTimeField()
    # source代表資料來源(同novel)
    # 0是本網站有的
    # 1是ptt
    # 2是起點中文網
    source = models.PositiveSmallIntegerField(default=0)
    # 20180620新增欄位
    is_delete = models.BooleanField(default=True)


# class NovelComment(models.Model):
#     class Meta:
#         db_table = 'novel_comment'
#     user = models.CharField(max_length=30)
#     comment = models.CharField(max_length=200)
#     book_title = models.CharField(max_length=30)
#     update_time = models.DateTimeField()

# 20180608 bee在line上說要新增的使用者資料表
class User(models.Model):
    class Meta:
        db_table = 'novel_user_bee'
    nick_name = models.CharField(max_length=20)
    fb_id = models.CharField(max_length=16)
    token = models.UUIDField() #只有這個在mysql中符合char(32)
    create_time = models.DateTimeField(auto_now_add=True)
    today_api_count = models.PositiveSmallIntegerField(default=0)
    last_login_time = models.DateTimeField() #資料庫原本的是timestamp [current_timestamp()]，但這個應該不是在這裡弄
    total_api_count = models.PositiveIntegerField(default=0) #資料庫原本的是double unsigned [0]，改int


