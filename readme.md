#如何修改資料表架構
1. 請打開novel/models.py，並參考django官方文件想辦法修改XD
2. 開terminal(請確保terminal 有python和django)
3. 進到分享給你的專案資料夾   然後輸入python manage.py makemigrations
4. 然後再輸入python manage.py migrate   完成

#如何新增初始資料到DB (利用yaml方式)
1. 請尋找novel資料夾裡面的fixtures   若沒有請自行新增
2. 請新增一個yaml檔案(記得副檔名要打)   假設檔名為xxx.yaml
3. 仿照iniclass.yaml的內容將想要的初始資料輸入進去
4. 請打開有django環境的terminal，並輸入 python manage.py loaddata xxx.yaml

#當DB格式混亂的時候，有個簡單處理方法
1. 備份有資料的那幾頁，變成sql檔(資料庫介面可以直接匯出)
2. 把migrations資料夾的所有內容砍掉，就再models.py的資料夾下(如果之前有執行過migrate指令的話就會出現此資料夾)
3. 進到資料庫頁面，把novel資料庫砍光，再重建一個novel資料庫
4. 接著進行"如何修改資料表架構"即可
2. 最後匯入那個sql檔即可，可用取代功能直接修改sql

# 砍掉重練的時候，輸入python manage.py makemigrations後出現No change detected問題發生如何解決 (20180620發生)
1. 砍掉所有_pycache_資料夾
2. 砍掉所有migrations資料夾
3. 開terminal(python和django的環境要有)，輸入python manage.py makemigrations novel (novel是App名稱)
4. 承上，理論上會看到models.py裡面建立的東西的名稱被terminal print出來
5. 再輸入python manage.py migrate   完成
